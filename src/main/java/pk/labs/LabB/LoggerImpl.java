package pk.labs.LabB;

import org.aspectj.lang.JoinPoint;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.Logger;

@Component("loggerImpl")
public class LoggerImpl{
	
        private Logger logger;
	
        void setLogger(Logger logger)
	{
		this.logger =logger;
	}
	
	
        public void entrance(JoinPoint joinPoint) 
        {
		if(logger!=null)
                {
			logger.logMethodEntrance(joinPoint.getSignature().getName(), joinPoint.getArgs());
                }
	}
        
        public void exit(JoinPoint joinPoint, Object exitResult) 
        {
		if(logger!=null)
                {
			logger.logMethodExit(joinPoint.getSignature().getName(), exitResult);
                }
	}
}
