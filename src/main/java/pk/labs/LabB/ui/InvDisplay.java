/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabB.ui;

import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.Display;
/**
 *
 * @author Marcin
 */
@Component("invDisplay")
public class InvDisplay implements Negativeable{

    Utils utils;
    
    public InvDisplay() {
       this.utils = new Utils();
    }
    
    
    @Override
    public void negative() {
        this.utils.negateComponent(((Display) AopContext.currentProxy()).getPanel());
    }
    
}
