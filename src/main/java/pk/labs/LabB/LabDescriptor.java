package pk.labs.LabB;

import pk.labs.LabB.Contracts.*;
import pk.labs.LabB.*;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = DisplayImpl.class.getName();
    public static String controlPanelImplClassName = ControlPanelImpl.class.getName();


    public static String mainComponentSpecClassName = EkspressFunkcje.class.getName();
    public static String mainComponentImplClassName = EkspresDoKawy.class.getName();
    public static String mainComponentBeanName = "ekspres";

    // endregion

    // region P2
    public static String mainComponentMethodName = "Wypisz";
    public static Object[] mainComponentMethodExampleParams = new Object[] { "test" };
    // endregion

    // region P3
    public static String loggerAspectBeanName = "loggerImpl";
    // endregion
}
